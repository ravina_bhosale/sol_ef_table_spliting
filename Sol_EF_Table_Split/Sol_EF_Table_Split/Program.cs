﻿using Sol_EF_Table_Split.Repository.User;
using Sol_EF_Table_Split.User;
using Sol_EF_Table_Split.User.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Table_Split
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () => {

                IUserRepository userRepositoryObj = new UserRepository();

                IEnumerable<IUserEntity> listUserEntityObj =
                    await userRepositoryObj.Select(null);

            }).Wait();
        }
    }
}
