﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Table_Split.CommonRepository
{
   public interface IUserSelect<TEntity> where TEntity :class
    {
        Task<IEnumerable<TEntity>> Select(TEntity entityObj);
    }
}
