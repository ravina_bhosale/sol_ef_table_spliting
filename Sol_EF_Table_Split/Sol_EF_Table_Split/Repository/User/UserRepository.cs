﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sol_EF_Table_Split.User.Interface;
using Sol_EF_Table_Split.EF;
using Sol_EF_Table_Split.User;

namespace Sol_EF_Table_Split.Repository.User
{
    public class UserRepository : IUserRepository
    {
        #region declaration
        private UserDBEntities dbObj = null;
        #endregion

        #region constructor
        public UserRepository()
        {
            dbObj = new UserDBEntities();
        }
        #endregion

        #region Public method

        public async Task<IEnumerable<IUserEntity>> Select(IUserEntity entityObj)
        {
            try
            {
                return await Task.Run(() =>
                {
                    var getQuery =
                    dbObj
                    ?.tblUsersAlls
                    ?.AsEnumerable()
                    ?.Select(this.SelectData)
                    ?.ToList();

                    return getQuery;
                });
            }

            catch(Exception)
            {
                throw;
            }
          
        }
        #endregion

        #region Private Property
        private Func<tblUsersAll,IUserEntity> SelectData
        {
            get
            {
                return
                      (letblUserObj) => new UserEntity()
                      {
                          FirstName = letblUserObj.FirstName,
                          LastName = letblUserObj.LastName,
                          UserId = (int)letblUserObj.UserId,

                          UserLogin = new UserLoginEntity()
                          {
                              UserName = letblUserObj?.tblUserLogin?.UserName,
                              Password = letblUserObj?.tblUserLogin?.Password

                          },

                          UserCommunication = new UserCommunicationEntity()
                          {
                              EmailId = letblUserObj?.tblUserCommunication?.EmailId,
                              MobileNo = letblUserObj?.tblUserCommunication?.MobileNo
                          }
                      };
            }
        }
        #endregion
    }
}
