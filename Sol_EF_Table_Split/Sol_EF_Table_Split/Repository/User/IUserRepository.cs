﻿using Sol_EF_Table_Split.CommonRepository;
using Sol_EF_Table_Split.User.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Table_Split.Repository.User
{
   public interface IUserRepository : IUserSelect<IUserEntity>
    {

    }
}
